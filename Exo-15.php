<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php
    // Faites un tableau associatif avec 10 donateurs et 10 dons
    // 1. Triez le tableau par ordre alphabétique. 2. Calculer la somme total des dons.
    // 3. Affichez les 3 plus gros donateurs, ainsi que les dons qu'ils ont faits
    // 4. Si un nouveau donateur donne une plus grosse somme, il doit apparaitre en 1er

    $don = [
        'Kevin' => 1000,
        'Pierre' => 500,
        'Rosa' => 5000,
        'Jérome' => 1700,
        'Nathalie' => 240,
        'Pricilla' => 50,
        'Angelique' => 750,
        'Damien' => 490,
        'Philippe' => 250,
        'Claude' => 350,
    ];
    //  1  ////////////////////////////////////////////////////

    echo '<p>1 . Tri par ordre alphabétique:</p>';

    ksort($don);
    foreach ($don as $cle => $valeur) {
        echo $cle. ': ' . $valeur.'<br>';
}

    echo '<p>2 . Total des dons: '.array_sum($don).'</p>';

    //  2  ////////////////////////////////////////////////////

    echo '<p>3 . 3 plus gros donateurs:</p>';

    // 3  ////////////////////////////////////////////////////

    /* arsort($don);
    max($don);
    for ( $i = 0; $i < 3; $i++ ) {
        foreach ($don as $cle => $valeur) {
            echo $cle.': '.$i.'<br>';
        }
    } */

    rsort($don);
    $maxvaleur = array_slice($don, 0, 3);
    
    foreach ($don as $cle => $valeur) {
        echo $maxvaleur.'<br>';
    }
    var_dump($don);
    var_dump($maxvaleur);


    ?>
</body>
</html>