<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // Faites un tableau associatif avec les pièces de votre maison. 
    // Ajoutez la cuisine à la liste des pièces avec l'opérateur d'affectation.
    // Affichez la liste des pièces de votre maison
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    
    <?php
         $loft =[
             'Chambre' => 'Blanche et verte',
             'Salon' => 'Blanc et marron',
             'Cave' => 'Sombre',
             'Chiotte' => '...',
             'Bureau' => 'Dans la chambre',
        ];
        
        $loft['Cuisine'] = 'Blanche et marron';

        foreach ($loft as $cle => $valeur) {
            echo $cle.' '.$valeur.'<br>';
        }

    ?>
    
    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>