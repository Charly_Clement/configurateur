<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>    

    <?php

    // $x affiche un chiffre entre 0 et 2
    // Si $x = 0, afficher $x est égal 0, sinon si $x = 1, affichez $x est égal à 1,
    // sinon affichez $x est égal à 2
    
    ?>
    
    <!-- écrire le code après ce commentaire -->
    <?php
    
    $x = rand(0,2);
    echo $x.'<br>';
    if ($x==0) {
        echo 'x est égal à '.$x;
    }else if ($x==1) {
        echo 'x est égal à '.$x;
    }else {
        echo 'x est égal à '.$x;
    }
    
    ?>
    <!-- écrire le code avant ce commentaire -->

</body>
</html>