<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Poiret+One&display=swap" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="configurateur.css" />
    <title>Configurateur</title>
</head>    
<body>
    



    <img src="img/header.jpg" alt="logo">
    <h1>Configurer votre véhicule</h1>

    <!--FORMULAIRE-->
    <form method="GET">

        <div>
            <h3>Choisissez votre Modèle</h3>
            <select name="modele">
                <option value="."></option>
                <option value="Modèle S">Modèle S</option>
                <option value="Modèle 3">Modèle 3</option>
            </select>
        </div>

        <div>
            <h3>Choisissez votre Couleur</h3>
            <select name="couleur">
                <option value="."></option>
                <option value="Blanc">Blanc</option>
                <option value="Rouge">Rouge</option>
                <option value="Bleu">Bleu</option>
            </select>
        </div>
        <div>
            <h3>Choisissez votre Puissance</h3>
            <select name="puissance">
                <option value="."></option>
                <option value="Autonomie">Autonomie</option>
                <option value="Performance">Performance</option>
            </select>
        </div>
        <div>
            <h3>Choisissez vos Options</h3>
            <select name="options">
                <option value="."></option>
                <option value="De série">De série</option>
                <option value="Full option">Full option</option>
            </select>
        </div>

            <input type="submit" value="Vivez votre rêve"><br>

    </from>
    <!--FORMULAIRE / End-->

    <?php include 'formulaire.php' ?>


<!-- COUCOU CHARLY!!!!!!!!! JAI HESITE A FAIRE DES TRUCS TAS DE LA CHANCE!!!!!!! -->

</body>
</html>