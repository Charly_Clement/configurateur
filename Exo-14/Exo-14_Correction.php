<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <link href="https://fonts.googleapis.com/css?family=Poiret+One&display=swap" rel="stylesheet">
    <style>
            #header{
            width: 100%;
            position: relative;
            top: 40px;
            opacity: 0;
            z-index: 1;
            animation: fondu 4s ease-in forwards 1s;
        }
        
        @keyframes fondu {
            0% { opacity: 0;}
            100% { opacity: 1;}
        }

        h1{
            font-family: 'Arial';
            font-size: 40px;
            margin: 50px auto;
            padding: 40px;
            text-align: center;
            text-transform: uppercase;
        }

        .container-line{
            display: flex;
            justify-content: space-around;
            width: 85%;
            margin: 0 auto;
            height: 150px;
        }
        
        .bloc{
            width: 30%;
            height: 90px;
            border-radius: 100px;
            box-shadow: 0 2px 8px grey;
            text-align: center;
            padding: 10px;
            padding-top: 20px;
            border: 3px solid transparent;
        }
        
        .bloc:hover{
            box-shadow: 0 2px 8px grey;
            border: 3px solid darkgrey;
            transition: 0.3s linear;
        }
        
        form{
            width: 100%;
            text-align: center;
            margin-bottom: 70px;
        }
        
        label{
            display: block;
            width: 100%;
            font-family: 'Arial';
            font-size: 23px;
        }
        
        select{
            width: 40%;
            font-family: 'Poiret One', cursive;
            font-size: 22px;
            height: 30px;
            position: relative;
            top:20px;
        }
        
        input[type=submit]{
            border: none;
            font-family: 'Poiret One', cursive;
            font-size: 25px;
            width: 30%;
            border-radius: 10px;
            box-shadow: 0 2px 8px grey;
            text-align: center;
            padding: 10px;
        }
        
        input[type=submit]:hover{
            border-radius: 30px;
            box-shadow: 0 5px 8px grey;
            transition: 0.3s linear;
        }

        #container-result {
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            background-color: white;
        }

        #container-result img{
            width: 100%;
            position: absolute;
            top: 100px;
            left: 0;
        }

        h2{
            text-align: center;
            font-family: 'Arial';
            font-size: 60px;
            position: relative;
            top: -30px;
            text-shadow: 0 2px 3px rgba(0,0,0,0.3);
        }

        #container-stat{
            position: relative;
            top:-60px;
            width: 100%;
            display: flex;
            justify-content: space-around;
            text-transform: uppercase;
            font-size: 30px;
        }

        #container-stat p {
            text-align: center;
            font-family: 'Poiret One', cursive;
            text-shadow: 0 2px 3px rgba(0,0,0,0.2);
        } 
</style>
</head>    
<body>
    
   <!-- écrire le code après ce commentaire -->

    <img src="/Exo-14/img/header.jpg" alt="" id="header">

    <h1>Je configure ma tesla</h1>
   
    <form methode="GET">
     <div class="container-line">
      <div class="bloc">
          <label for="modele">Choisissez votre modèle</label>
          <select name="modele">
               <option value="vide"></option>
               <option value="modelS">Model S</option>
               <option value="model3">Model 3</option>
           </select>
       </div>
       
       <div class="bloc">
           <label for="couleur">Choisissez votre couleur</label>
           <select name="couleur">
               <option value="vide"></option>
               <option value="Blanc">Blanc</option>
               <option value="Bleu">Bleu</option>
               <option value="Rouge">Rouge</option>
           </select>
       </div>
     </div> 
      
      
      <div class="container-line">
       <div class="bloc">
           <label for="puissance">Choisissez votre puissance</label>
           <select name="puissance">
               <option value="vide"></option>
               <option value="Autonomie">Autonomie</option>
               <option value="Performance">Performance</option>
           </select>
       </div>
       
       <div class="bloc">
           <label for="option">Choisissez les options</label>
           <select name="option">
               <option value="vide"></option>
               <option value="serie">De série</option>
               <option value="full">Full option</option>
           </select>
       </div>
      </div>   
       
       <input type="submit" name="submit" value="Vivez votre rêve !">
   </form> 
    
    <?php

    $z = 0;
        
    $modele = $_GET['modele'];
    $couleur = $_GET['couleur'];
    $puissance = $_GET['puissance'];
    $option = $_GET['option'];
    
    $statsModelSAutonomie = ['Autonomie' => '610km', 'Vitesse' => "250Km/h", '0-100' => '3,8s', 'Prix' => 91000];
    $statsModelSPerformance = ['Autonomie' => '593km', 'Vitesse' => "261Km/h", '0-100' => '2,6s', 'Prix' => 107700];
    
    $statsModel3Autonomie = ['Autonomie' => '560km', 'Vitesse' => "233Km/h", '0-100' => '4,6s', 'Prix' => 57800];
    $statsModel3Performance = ['Autonomie' => '530km', 'Vitesse' => "261Km/h", '0-100' => '3,4s', 'Prix' => 64890];
    
    $couleurModelS = ['Blanc' => 0, 'Bleu' => 1600, 'Rouge' => 2600];
    $couleurModel3 = ['Blanc' => 0, 'Bleu' => 1000, 'Rouge' => 2000];
    
    $listOption = ['Interieur' => 2000, 'Jantes Aluminium' => 3000, 'Auto-pilot' => 6500]; // 11 500
    
 
    
    // Choix modele
    if($modele == "modelS"){
        $name = "Model S";
    } elseif ($modele == "model3"){
        $name = "Model 3";
    } else {
        $name = "";
    }
  
    
    // Séparation modele pour l'image
    $modelevoiture = $modele;
   
    
    // Prix de la peinture
    if($modele == "modelS"){
        switch($couleur){
        case'Blanc':
            $prixpeinture = $couleurModelS['Blanc'];
        break;
        case'Bleu':
            $prixpeinture = $couleurModelS['Bleu'];
        break;
        case'Rouge':
            $prixpeinture = $couleurModelS['Rouge'];
        break;
        }
    } else{
        switch($couleur){
        case'Blanc':
            $prixpeinture = $couleurModel3['Blanc'];
        break;
        case'Bleu':
            $prixpeinture = $couleurModel3['Bleu'];
        break;
        case'Rouge':
            $prixpeinture = $couleurModel3['Rouge'];
        break;
        }
    }
    
    // Récupération du nom du tableau
    $affichageTableauModel = $modele .= $puissance;
    
    
    // Prix Option
    if($option == "serie"){
        $prixoption = 0;
    } else {
        $prixoption = array_sum($listOption);
    }
    
    // Récuperation du prix du model
    switch($affichageTableauModel){
        case'modelSAutonomie':
            $prixVoitureSeul = $statsModelSAutonomie['Prix'];
        break;
        case'modelSPerformance':
            $prixVoitureSeul = $statsModelSPerformance['Prix'];
        break;
        case'model3Autonomie':
            $prixVoitureSeul = $statsModel3Autonomie['Prix'];
        break;
        case'model3Performance':
            $prixVoitureSeul = $statsModel3Performance['Prix'];
        break;
    }
    
    
    // Calcule du cout total
    $coutTotal = $prixVoitureSeul + $prixpeinture + $prixoption;
    
    // Séparation des milliers
    $coutTotal = number_format($coutTotal, '0',' ',' ');
        
    //Ajout du signe euro
    $coutTotal .= " €";
    
    // Modification du prix final du model dans le tableau avant de l'afficher
    switch($affichageTableauModel){
        case'modelSAutonomie':
            $statsModelSAutonomie['Prix'] = $coutTotal;
        break;
        case'modelSPerformance':
            $statsModelSPerformance['Prix'] = $coutTotal;
        break;
        case'model3Autonomie':
            $statsModel3Autonomie['Prix'] = $coutTotal;
        break;
        case'model3Performance':
            $statsModel3Performance['Prix'] = $coutTotal;
        break;
    }
    
    // Bonus : passage de l'image Tesla sous le container-stats
    if(!empty($_GET['submit'])){ 
        $z = 2;
    }
                                                    
?>

   <div style="z-index:<?php echo $z;?>;" id="container-result">
            
             <img src="/Exo-14/img/<?php echo $modelevoiture; ?>/<?php echo $couleur; ?>.png" alt="">
            
            <h2><?php echo $name; ?></h2>
            <div id="container-stat">
                <?php 
                switch($affichageTableauModel){
                    case'modelSAutonomie':
                        foreach($statsModelSAutonomie as $keys => $valeurs){
                        echo '<p>'. $keys .' : '. $valeurs .'</p>';
                        }
                    break;
                    case 'modelSPerformance':
                        foreach($statsModelSPerformance as $keys => $valeurs){
                        echo '<p>'. $keys .' : '. $valeurs .'</p>';
                        }
                    break;
                        case'model3Autonomie':
                        foreach($statsModel3Autonomie as $keys => $valeurs){
                        echo '<p>'. $keys .' : '. $valeurs .'</p>';
                        }
                    break;
                    case 'model3Performance':
                        foreach($statsModel3Performance as $keys => $valeurs){
                        echo '<p>'. $keys .' : '. $valeurs .'</p>';
                        }
                    break;    
                } ?>
            </div>
        </div>
    
    <!-- écrire le code avant ce commentaire -->

</body>
</html>