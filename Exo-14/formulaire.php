<?php

    $recup_modele    = $_GET['modele'];
    $recup_couleur   = $_GET['couleur'];
    $recup_puissance = $_GET['puissance'];
    $recup_options   = $_GET['options'];
    

    $modele = [
            'modele_1' => 'modele S',
            'modele_2' => 'modele 3',    
    ];

    $couleur_modele_S = [
            'bleu' => 1600,
            'rouge' => 2600,
    ];
    $couleur_modele_3 = [
            'Bleu' => 1000,
            'Rouge' => 2000,
    ];

    $modele_s_autonomie = [
            'Autonomie ' => '610 km ',
            'Vitesse ' => '250 km/h ',
            '0-100 ' => '3,8 sec ',
            'Prix ' => 91000,
    ];

    $modele_s_performance = [
            'Autonomie ' => '593 km',
            'Vitesse ' => '261 km/h',
            '0-100 ' => '2,6 sec',
            'Prix ' => 107700,
    ];

    $modele_3_autonomie = [
            'Autonomie ' => '560 km',
            'Vitesse ' => '233 km/h',
            '0-100 ' => '4,6 sec',
            'Prix ' => 57800,
    ];

    $modele_3_performance = [
            'Autonomie ' => '530 km',
            'Vitesse ' => '261 km/h',
            '0-100 ' => '3,4 sec',
            'Prix ' => 64890,
    ];

    $option = [
            'Intérieur' => 2000,
            'Jantes Aluminium' => 3000,
            'Auto-pilot' => 6500,
    ];

    echo '<h1 style="align-text:center">'.$recup_modele.'</h1>';

    if ($recup_options == 'De série') {
        $total_Prix_Options = 0;
    }else {
        $total_Prix_Options = array_sum($option);
    }

    if ($recup_couleur == 'Blanc') {
        $total_Prix_Couleur = 0;
    }else if ($recup_couleur == 'Bleu') {
        if ($recup_modele == 'Modèle S') {
            $total_Prix_Couleur = 1600;
        }else {
            $total_Prix_Couleur = 1000;
        }
    }else {
        if ($recup_modele == 'Modèle S') {
            $total_Prix_Couleur = 2600;
        }else {
            $total_Prix_Couleur = 2000;
        }
    }

    $modele_s_autonomie  ['Prix ']   = $total_Prix_Options + $total_Prix_Couleur + $modele_s_autonomie  ['Prix '];
    $modele_s_performance['Prix ']   = $total_Prix_Options + $total_Prix_Couleur + $modele_s_performance['Prix '];
    $modele_3_autonomie  ['Prix ']   = $total_Prix_Options + $total_Prix_Couleur + $modele_3_autonomie  ['Prix '];
    $modele_3_performance['Prix ']   = $total_Prix_Options + $total_Prix_Couleur + $modele_3_performance['Prix '];


    //.....AUTONOMIE OU PERFORMANCE.....AUTONOMIE OU PERFORMANCE

    if ($recup_modele == "Modèle S" && $recup_puissance == "Autonomie") {
        foreach ($modele_s_autonomie as $cle => $valeur) {
            echo '<h3>'.$cle.$valeur.'</h3>';
        }
    }else if ($recup_modele == "Modèle S" && $recup_puissance == "Performance") {
        foreach ($modele_s_performance as $cle => $valeur) {
            echo '<h3>'.$cle.$valeur.'</h3>';
        }
    }else if ($recup_modele == "Modèle 3" && $recup_puissance == "Autonomie") {
        foreach ($modele_3_autonomie as $cle => $valeur) {
            echo '<h3>'.$cle.$valeur.'</h3>';
        }
    }else {
        foreach ($modele_3_performance as $cle => $valeur) {
            echo '<h3>'.$cle.$valeur.'</h3>';
        }
    }

    //.....PHOTO.....PHOTO.....PHOTO.....PHOTO.....PHOTO.....PHOTO.....PHOTO

    if ($recup_modele == "Modèle S" && $recup_couleur == "Blanc") {
        echo '<img src="img/modelS/Blanc.png">';
    }else if ($recup_modele == "Modèle S" && $recup_couleur == "Rouge") {
        echo '<img src="img/modelS/Rouge.png">';
    }else if ($recup_modele == "Modèle S" && $recup_couleur == "Bleu") {
        echo '<img src="img/modelS/Bleu.png">';
    }

    if ($recup_modele == "Modèle 3" && $recup_couleur == "Blanc") {
        echo '<img src="img/model3/Blanc.png">';
    }else if ($recup_modele == "Modèle 3" && $recup_couleur == "Rouge") {
        echo '<img src="img/model3/Rouge.png">';
    }else if ($recup_modele == "Modèle 3" && $recup_couleur == "Bleu") {
        echo '<img src="img/model3/Bleu.png">';
    }

    ?>