<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tableaux PHP - Partie 2 - Exercice</title>
</head>
<body>
<h1>Exercices Tableaux PHP</h1>
<?php
    
$marque = ["Dell", "HP", "Asus", "Apple", "Msi"];
    
$marquePrix = ["Dell" => 1700, "HP" => 1000, "Asus" => 800, "Apple" => 2500, "Msi" => 2000];

$complet = [
    "Dell" => [
        "Processeur" => 'i7',
        "Mémoire RAM" => 12,
        "Disque dur" => 2000,
        "Taille d'écran" => 17,
        "Prix" => 1700
    ],
    "HP" => [
        "Processeur" => 'i5',
        "Mémoire RAM" => 8,
        "Disque dur" => 1000,
        "Taille d'écran" => 16,
        "Prix" => 1000
    ],
    "Asus" => [
        "Processeur" => 'i3',
        "Mémoire RAM" => 6,
        "Disque dur" => 500,
        "Taille d'écran" => 15,
        "Prix" => 800
    ],
    "Apple" => [
        "Processeur" => 'i7',
        "Mémoire RAM" => 16,
        "Disque dur" => 2000,
        "Taille d'écran" => 13,
        "Prix" => 2500
    ],
    "Msi" => [
        "Processeur" => 'i7',
        "Mémoire RAM" => 16,
        "Disque dur" => 2000,
        "Taille d'écran" => 17,
        "Prix" => 2000
    ],
];

// Ajout COMPAQ
$marque[] = 'Compaq';
    $marquePrix['Compaq'] = 600;
        $complet['Compaq'] = [
                'Processeur' => 'i5',
                'Mémoire RAM' => 6,
                'Disque dur' => 1000,
                "Taille d'écran" => 17,
                'Prix' => 2000,
        ];

// Nombre de marques dans les tableaux
echo 'Il y a '.count($marque).' marques dans le tableau $marque.<br>';
echo 'Il y a '.count($marquePrix).' marques dans le tableau $marquePrix.<br>';
echo 'Il y a '.count($complet).' marques dans le tableau $complet.<br>';

// Prix Disques dur
$complet[0][2] = 1500;
$complet['Apple']['Disque dur'] = 1500;
$complet[4][2] = 1500;

// Prix Dell
$marquePrix['Dell'] = 1500;

// Afficher Marque et Prix de tous les modèles 

    var_dump($complet[0]['Prix']);

 
/* Dans tout les tableaux, ajouter le PC de marque 'Compaq' avec un processeur i5, 6Go de RAM,
    disque dur de 1000Go, un écran de 17 pouces à 600€. 
 Affichez le nombre de marques dans le tableau
 Changez la capacité des disques de 2000 gigas en 1500 gigas
 Grâce à une promotion, le prix du Dell passe à 1500€
 Faire une boucle afin d'afficher la marque et le prix de tous les modèles
 */